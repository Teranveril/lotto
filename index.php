<!DOCTYPE html>
<html lang="pl-PL">

<head>
</head>

<body>
    <?php
    require './class.php';


// Klasa ,,lotto" przyjmuje dwa parametry: Pierwsza to ilość kól w maszynie losującej. Druga to ilość kól które musi wyłapać
// Metody dla klasy ,,lotto":
// set_rewards - wyznacza nagrode za: tutaj tablica asocjacyjna. Wyznaczamy nagrodę za konkretną ilość trafionych kul
// generate - ilość losowań
// get_draw - metoda ukazująca trafione liczby ( do zastosowania z metodą check u gracza)
// rewards - ogłoszenie nagród (dla gracza)

// Klasa ,,Player" przyjmuje jeden parametr: Nazwa gracza
// Metody dla klasy ,,Player":
// addtickets - zakup tokenów dla gracza na konkretne losowanie
// check - sprawdza wygraną 


    $lotto = new lotto(48, 6);

    $lotto->set_rewards(['2' => '500 zł', '3' => '1000 zł', '4' => '25000 zł', '5' => 'Samochód Ford Focus']);


    $player1 = new Player('Adam');
    $player1->addtickets($lotto->generate(5));
    $player1->check($lotto->get_draw(), $lotto->rewards());




    ?>
</body>

</html>