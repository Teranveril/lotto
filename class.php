
<?php




class Player
{

    public $player;
    public $tickets;
    public $catched_number;
    public $catched_numbers = [];
    public $reward = [];


    public function __construct($player)
    {
        $this->player = $player;
    }

    public function addtickets($ticket)
    {
        $this->tickets = $ticket;
    }

    public function check($drew, $reward)
    {

        echo "Wylosowane liczby - " . implode(', ', $drew);
        foreach ($drew as $key => $value) {

            $this->catched_number[] = $value;
        }

        $this->reward = $reward;

        foreach ($this->tickets as $key => $value) {
            $catched_numbers = [];
            echo " <br /> $this->player postawiłeś kupon na nasępujące liczby - ";
            foreach ($value as $value2) {
                echo "$value2, ";
            }
            echo " <br />";
            $array_intersect = array_intersect($value, $this->catched_number);
            $catched_numbers = count($array_intersect);
            $this->catched_numbers = $catched_numbers;
            if ($array_intersect) {
                echo "Trafione cyfry to - ";
            }
            foreach ($array_intersect as $key => $value4) {
                $value5[] = $value4;
            }
            if ($array_intersect) {
                echo implode(', ', $value5);
            }
            if (isset($reward[$this->catched_numbers])) {
                echo ' Wygrałeś ' . $reward[$this->catched_numbers] . " Trafiłeś {$this->catched_numbers} ";
            } else {
                echo ' Graj dalej';
            }
        }
    }
}

class Lotto
{
    public $pool;
    public $balls_to_catch;
    public $reward = [];
    public $get_reward;
    public $draw;


    public function __construct($pool, $balls_to_catch)
    {
        $this->pool = $pool;
        $this->range = range(1, $pool);
        $this->balls_to_catch = $balls_to_catch;
        $this->draw = new Draw();
    }

    public function set_rewards(array $reward)
    {
        $this->reward = $reward;
    }

    public function rewards()
    {
        return $this->reward;
    }

    public function generate($how_much)
    {
        for ($i = 0; $i < $how_much; $i++) {

            $catched_ticket[] = $this->draw->drew($this->balls_to_catch, $this->range);
        }
        return $catched_ticket;
    }

    public function get_draw()
    {
        return $this->draw->drew($this->balls_to_catch, $this->range);
    }
}

class Draw
{
    public function drew($balls_to_catch, $range)
    {
        $drew_number = [];
        while (count($drew_number) < $balls_to_catch) {
            $draw = $range;
            shuffle($draw);
            $number = array_shift($draw);
            $drew_number[$number] = $number;
        }
        return $drew_number;
    }
}
